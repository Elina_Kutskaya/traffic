package com.example.trafficlight;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ConstraintLayout layout = findViewById(R.id.con_layout);
        Button butRed = findViewById(R.id.buttonRed);
        Button butYellow = findViewById(R.id.buttonYellow);
        Button butGreen = findViewById(R.id.buttonGreen);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.buttonRed:
                        color = getResources().getColor(R.color.colorRed);
                        layout.setBackgroundColor(color);
                        break;
                    case R.id.buttonYellow:
                        color = getResources().getColor(R.color.colorYellow);
                        layout.setBackgroundColor(color);
                        break;
                    case R.id.buttonGreen:
                        color = getResources().getColor(R.color.colorGreen);
                        layout.setBackgroundColor(color);
                        break;
                }
            }
        };
        butRed.setOnClickListener(onClickListener);
        butYellow.setOnClickListener(onClickListener);
        butGreen.setOnClickListener(onClickListener);
    }

    @Override
    public void onResume(){
        super.onResume();
        resetUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color",color);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        color = savedInstanceState.getInt("color");
    }


    private void resetUI() {
        findViewById(R.id.con_layout).setBackgroundColor(color);
    }
}
